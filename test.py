#! /usr/bin/env python
#import cppyy
## this is a slightly modified (no main) version of https://gitlab.cern.ch/snippets/110
#cppyy.gbl.gInterpreter.Declare('#include "func.h"')


class Functor(object):
    def __init__(self, expression, name = None, types = None):
        if types is not None:
            if name is not None:
                expression = "single_signature<{}>({!r}, {})".format(types, name, expression)
            else:
                expression = "single_signature<{}>({})".format(types, expression)
        elif name is not None:
            expression = "func({!r}, {})".format(name, expression)
        self.expression = expression
        self.name = name

    def __add__(self, other):
        return Functor("({!r}) + ({!r})".format(self, other))
    def __radd__(self, other):
        return Functor("({!r}) + ({!r})".format(other, self))

    def __sub__(self, other):
        return Functor("({!r}) - ({!r})".format(self, other))
    def __rsub__(self, other):
        return Functor("({!r}) - ({!r})".format(other, self))

    def __mul__(self, other):
        return Functor("({!r}) * ({!r})".format(self, other))
    def __rmul__(self, other):
        return Functor("({!r}) * ({!r})".format(other, self))

    def __div__(self, other):
        return Functor("({!r}) / ({!r})".format(self, other))
    def __rdiv__(self, other):
        return Functor("({!r}) / ({!r})".format(other, self))

    def compose(self, other):
        return Functor("compose({!r}, {!r})".format(self, other))

    def __str__(self):
        if self.name:
            return self.name
        return self.expression

    def __repr__(self):
        return str(self)

    def key(self):
        from hashlib import sha1
        return 'fctr_{}'.format(sha1(repr(self)).hexdigest())

    @property
    def _compiled_function(self):
        if self._cached is None:
            key = self.key()
            if not hasattr(cppyy.gbl, key):
                assert cppyy.gbl.gInterpreter.Declare('auto {} = ({!r}).create();'.format(key, self)), \
                    'failed to compile: {}'.format(self)
            self._cached = getattr(cppyy.gbl, key)
        return self._cached

    def __call__(self, *args, **kwargs):
        return self._compiled_function(*args, **kwargs)





def main():
    ID = Functor('id', 'id', 'int')
    sqr = ID * ID
    cube = ID * ID * ID
    f = cube
    g = ID + 2
    print(ID)
    print(sqr)
    print(cube)
    print(g)

if __name__ == '__main__':
    main()

