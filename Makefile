test: Makefile functor.h func-py.h main.cpp
	$(CXX) -O3 -std=c++14 -Wall -Wextra -Wpedantic -g main.cpp -o test

clean:
	rm -f test

.PHONY: clean
