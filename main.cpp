#include <iostream>
#include <sstream>
#include "functor.h"

template <class... Args>
auto types() { return __PRETTY_FUNCTION__; }
template <class... Args>
auto types(Args&&...) { return types<Args&&...>(); }

using namespace functors;

Identity id;

class Foo {
  private:
    enum class State : char {
      valid = 0,
      moved = 1,
      destroyed = 2
    };
    volatile State state = State::valid;
    std::ostream& log() const {
      return std::cerr << "[" << this << "] ";
    }
    explicit Foo(State state) : state(state) {}
  public:
    void ensureValid() const {
      if (state == State::valid) return;
      std::stringstream ss;
      ss << "[" << this << "] Used after ";
      if (state == State::moved) ss << "move";
      if (state == State::destroyed) ss << "destruction";
      std::cerr << ss.str() << std::endl;
      //throw std::runtime_error(ss.str());
    }
  private:
    static void consume(Foo const& foo) {
      foo.ensureValid();
    }
    static void consume(Foo&& foo) {
      foo.ensureValid();
      foo.state = State::moved;
    }

    template <class T>
    using requires_foo = functors::details::if_valid<T, std::enable_if_t<std::is_same<std::decay_t<T>, Foo>::value>>;
    template <class U, class V>
    using both = std::enable_if_t<std::is_same<std::decay_t<U>, Foo>::value && std::is_same<std::decay_t<V>, Foo>::value, Foo>;
  public:
    Foo() { log() << "Default construction" << std::endl; }
    Foo(const Foo& foo) { consume(foo); log() << "Copy construction from " << &foo << std::endl; }
    Foo(Foo&& foo) { consume(std::move(foo)); log() << "Move construction from " << &foo << std::endl; }
    Foo& operator=(const Foo& foo) {
      consume(foo);
      log() << "Copy assignment from " << &foo << std::endl; 
      return *this;
    }
    Foo& operator=(Foo&& foo) {
      consume(std::move(foo));
      log() << "Move assignment from " << &foo << std::endl; 
      return *this;
    }

    ~Foo() {
      log() << "destruction" << std::endl;
      state = State::destroyed;
    }

    template <class U, class V>
    friend both<U, V> operator+(U&& a, V&& b) {
      consume(std::forward<U>(a));
      consume(std::forward<V>(b));
      Foo r = Foo(State::valid);
      r.log() << "+ construction from " << &a << " and " << &b << "\t\t" << __PRETTY_FUNCTION__ << std::endl;
      return r;
    }
    template <class U, class V>
    friend both<U, V> operator-(U&& a, V&& b) {
      consume(std::forward<U>(a));
      consume(std::forward<V>(b));
      Foo r = Foo(State::valid);
      r.log() << "- construction from " << &a << " and " << &b << "\t\t" << __PRETTY_FUNCTION__ << std::endl;
      return r;
    }
    template <class U, class V>
    friend both<U, V> operator*(U&& a, V&& b) {
      consume(std::forward<U>(a));
      consume(std::forward<V>(b));
      Foo r = Foo(State::valid);
      r.log() << "* construction from " << &a << " and " << &b << "\t\t" << __PRETTY_FUNCTION__ << std::endl;
      return r;
    }
    template <class U, class V>
    friend both<U, V> operator/(U&& a, V&& b) {
      consume(std::forward<U>(a));
      consume(std::forward<V>(b));
      Foo r = Foo(State::valid);
      r.log() << "/ construction from " << &a << " and " << &b << "\t\t" << __PRETTY_FUNCTION__ << std::endl;
      return r;
    }

    explicit operator bool() const {
      ensureValid();
      log() << "bool conversion" << std::endl;
      return true;
    }
    template <class T, class = std::enable_if_t<std::is_same<std::decay_t<T>, Foo>::value>>
    friend std::ostream& operator<<(std::ostream& out, T&& foo) {
      consume(std::forward<T>(foo));
      return out << "Foo(" << &foo << ")";
    }
};

int main() {
  auto f = compose(compose(id+id, id*id, id&&id, id||id) + compose((id + id + Foo{} * id * id), id+id, id || id, id && id));
  auto g = compose(id + id, id + id);

  std::cout << f << std::endl;
  //std::cout << f(1) << std::endl;
  std::cout << f(Foo{}) << std::endl;
  std::cout << types<decltype(f(Foo{}))>() << std::endl;

  return 0;
}
