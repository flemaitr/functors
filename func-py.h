#include "functor.h"

namespace functors_py {
  template <class F, class... Args> class SingleSignature;

  namespace details {
    template <class... Args>
    struct args {
      template <class F>
      using bind_t = SingleSignature<F, Args...>;

      template <class F>
      constexpr static bind_t<F> bind(F&& f) {
        return std::forward<F>(f);
      }
    };

    template <class F, class G> struct combine;
    template <class F1, class F2, class... Args1, class... Args2>
    struct combine<SingleSignature<F1, Args1...>, SingleSignature<F2, Args2...>>
      : args<std::common_type_t<Args1, Args2>...>
    {};

    template <class T, class F, class... Args>
    struct combine<T, SingleSignature<F, Args...>>
      : args<Args...>
    {};
    template <class T, class F, class... Args>
    struct combine<SingleSignature<F, Args...>, T>
      : args<Args...>
    {};

    template <class>
    struct is_single_signature : std::false_type {};
    template <class F, class... Args>
    struct is_single_signature<SingleSignature<F, Args...>> : std::true_type {};

    template <class...>
    struct any_is_single_signature : std::false_type {};
    template <class Head, class... Tail>
    struct any_is_single_signature<Head, Tail...> : std::conditional_t<is_single_signature<Head>::value, std::true_type, any_is_single_signature<Tail...>> {};

    template <class... Ts>
    using enable_any_is_single_signature = std::enable_if_t<any_is_single_signature<std::decay_t<Ts>...>::value>;

    template <class T>
    using requires_single_signature = functors::details::if_valid<T, enable_any_is_single_signature<T>>;

    template <class...> struct last;
    template <class Head> struct last<Head> { using type = Head; };
    template <class H1, class H2, class... Tail> struct last<H1, H2, Tail...> : last<H2, Tail...> {};

    namespace {
      template <class T, std::enable_if_t<!is_single_signature<std::decay_t<T>>::value, std::nullptr_t> = nullptr>
      constexpr decltype(auto) extract(T&& t) {
        return std::forward<T>(t);
      }

      template <class F, std::enable_if_t<is_single_signature<std::decay_t<F>>::value, std::nullptr_t> = nullptr>
      constexpr decltype(auto) extract(F&& f) {
        return std::forward<F>(f).get();
      }
    }
  }

  template <class F, class... Args>
  struct SingleSignature : private F {
    template <class G, class = decltype(F(std::declval<G>())(std::declval<Args>()...))>
    constexpr SingleSignature(G&& g) : F(std::forward<G>(g)) {}

    constexpr SingleSignature() = delete;
    constexpr SingleSignature(SingleSignature const& ) = default;
    constexpr SingleSignature(SingleSignature      &&) = default;
    SingleSignature& operator=(SingleSignature const& ) = default;
    SingleSignature& operator=(SingleSignature      &&) = default;
    ~SingleSignature() = default;

    constexpr F const&  get() const&  { return *this; }
    constexpr F      && get()      && { return *this; }

    using args = details::args<Args...>;
    using func_t = F;
    using type = functors::Functor<decltype(std::declval<F const&>()(std::declval<Args>()...))(Args...)>;

    constexpr type create() const&  { return get(); }
    constexpr type create()      && { return get(); }

    constexpr decltype(auto) operator()(Args... args) const noexcept(noexcept(std::declval<F const&>())) {
      return get()(static_cast<Args>(args)...);
    }

    friend std::ostream& operator<<(std::ostream& out, SingleSignature const& f) {
      return out << f.get();
    }

  };

  template <class... Args, class T>
  constexpr auto single_signature(T&& t) {
    decltype(auto) f = functors::details::ensure_func(details::extract(std::forward<T>(t)));
    using F = std::decay_t<decltype(f)>;
    return SingleSignature<F, Args...>(f);
  }

  template <class... Args, class S, class T>
  constexpr auto single_signature(S&& name, T&& t) {
    auto f = functors::func(std::forward<S>(name), details::extract(std::forward<T>(t)));
    using F = std::decay_t<decltype(f)>;
    return SingleSignature<F, Args...>(std::move(f));
  }

  template <class S, class F, class... Args>
  constexpr auto func(S&& name, SingleSignature<F, Args...> f) {
    return single_signature(std::forward<S>(name), std::move(f));
  }

  template <class F, class... Args>
  constexpr auto func(SingleSignature<F, Args...> f) {
    return f;
  }

#define DECLARE_UNOP(op) \
  template <class T, class = details::enable_any_is_single_signature<T>> \
  constexpr auto operator op(T&& t) {  \
    return T::args::bind(op std::forward<T>(t).get()); \
  }

#define DECLARE_BINOP(op) \
  template <class U, class V, class = details::enable_any_is_single_signature<U, V>> \
  auto operator op(U&& u, V&& v) { \
    return details::combine<std::decay_t<U>, std::decay_t<V>>::bind(details::extract(std::forward<U>(u)) op details::extract(std::forward<V>(v))); \
  }

  // Arithmetic
  DECLARE_UNOP(+)
  DECLARE_UNOP(-)
  DECLARE_BINOP(+)
  DECLARE_BINOP(-)
  DECLARE_BINOP(*)
  DECLARE_BINOP(/)
  DECLARE_BINOP(%)

  // Comparisons
  DECLARE_BINOP(==)
  DECLARE_BINOP(!=)
  DECLARE_BINOP(>)
  DECLARE_BINOP(<)
  DECLARE_BINOP(>=)
  DECLARE_BINOP(<=)
  
  // Logical operators
  DECLARE_BINOP(||)
  DECLARE_BINOP(&&)
  DECLARE_UNOP(!)

  // Bit operators
  DECLARE_UNOP(~)
  DECLARE_BINOP(&)
  DECLARE_BINOP(|)
  DECLARE_BINOP(^)

#undef DECLARE_UNOP
#undef DECLARE_BINOP

  template <class... Fs>
  constexpr auto compose(details::requires_single_signature<Fs>&&... fs) {
    return details::last<std::decay_t<Fs>...>::type::args::bind(compose(std::forward<Fs>(fs).get()...));
  }
} // namespace functors_py
