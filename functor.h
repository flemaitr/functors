#pragma once

#include <utility>
#include <type_traits>
#include <functional>
#include <memory>
#include <string>
#include <iosfwd>
#include <sstream>

namespace functors {
  namespace details {
    template <class T>
    struct is_empty_optimizable : std::integral_constant<bool, std::is_empty<T>::value && std::is_trivial<T>::value && std::is_default_constructible<T>::value> {};

    template <class T, class = void, class...>
    class wrapper {
      private:
        T t;
      public:
        template <class... Args, class = decltype(T(std::declval<Args&&>()...))>
        constexpr wrapper(Args&&... args) : t(std::forward<Args>(args)...) {}
        constexpr wrapper() = default;
        constexpr wrapper(wrapper const& ) = default;
        constexpr wrapper(wrapper      &&) = default;
        wrapper& operator=(wrapper const& ) = default;
        wrapper& operator=(wrapper      &&) = default;
        ~wrapper() = default;

        constexpr T      &  get()      &  noexcept { return t; }
        constexpr T      && get()      && noexcept { return t; }
        constexpr T const&  get() const&  noexcept { return t; }
        constexpr T const&& get() const&& noexcept { return t; }
    };
    template <class T, class... Dummy>
    class wrapper<T, std::enable_if_t<is_empty_optimizable<T>::value>, Dummy...> {
      public:
        template <class... Args, class = decltype(T(std::declval<Args&&>()...))>
        constexpr wrapper(Args&&... args) { (void)T(std::forward<Args>(args)...); }
        constexpr wrapper() = default;
        constexpr wrapper(wrapper const& ) = default;
        wrapper& operator=(wrapper const& ) = default;
        ~wrapper() = default;

        constexpr T get() const noexcept { return T{}; }
    };

    template <class T, class... Dummy>
    using wrap = wrapper<T, void, Dummy...>;

    template <class T, class = void>
    struct is_callable_helper {
      using type = std::false_type;
    };
    template <class T>
    struct is_callable_helper<T, std::enable_if_t<std::is_class<T>::value, void>> {
      private:
        struct Fallback { void operator()(); };
        struct Derived : T, Fallback { };

        template<typename U, U> struct Check;

        template <class>
        static std::true_type test(...);

        template <class C>
        static std::false_type test(Check<void (Fallback::*)(), &C::operator()>*);
      public:
        using type = decltype(test<Derived>(0));
    };

    template <class T>
    struct is_callable : is_callable_helper<T>::type {};

    template <class T> T& unmove(T&& t) noexcept { return t; }

    namespace {
      template <class T> constexpr T const& op_name(T const& val) { return val; }
      constexpr const char* op_name(std::negate<>       ) { return "-" ; }
      constexpr const char* op_name(std::plus<>         ) { return "+" ; }
      constexpr const char* op_name(std::minus<>        ) { return "-" ; }
      constexpr const char* op_name(std::multiplies<>   ) { return "*" ; }
      constexpr const char* op_name(std::divides<>      ) { return "/" ; }
      constexpr const char* op_name(std::modulus<>      ) { return "%" ; }
      constexpr const char* op_name(std::equal_to<>     ) { return "=="; }
      constexpr const char* op_name(std::not_equal_to<> ) { return "!="; }
      constexpr const char* op_name(std::greater<>      ) { return ">" ; }
      constexpr const char* op_name(std::less<>         ) { return "<" ; }
      constexpr const char* op_name(std::greater_equal<>) { return ">="; }
      constexpr const char* op_name(std::less_equal<>   ) { return "<="; }
      constexpr const char* op_name(std::logical_and<>  ) { return "&&"; }
      constexpr const char* op_name(std::logical_or<>   ) { return "||"; }
      constexpr const char* op_name(std::logical_not<>  ) { return "!" ; }
      constexpr const char* op_name(std::bit_and<>      ) { return "&" ; }
      constexpr const char* op_name(std::bit_or<>       ) { return "|" ; }
      constexpr const char* op_name(std::bit_xor<>      ) { return "^" ; }
      constexpr const char* op_name(std::bit_not<>      ) { return "~" ; }
    }

    template <class T, class...>
    using if_valid = T;

    template <class, class...> struct RecursiveHelper;
    template <class Dummy, class F>
    struct RecursiveHelper<Dummy, F> : private F {
      private:
        constexpr F const& f() const { return *this; }
      public:
        template <class G, class = decltype(F(std::declval<G&&>()))>
        constexpr RecursiveHelper(G&& g) : F(std::forward<G>(g)) {}
        constexpr RecursiveHelper() = default;
        constexpr RecursiveHelper(RecursiveHelper const& ) = default;
        constexpr RecursiveHelper(RecursiveHelper      &&) = default;
        RecursiveHelper& operator=(RecursiveHelper const& ) = default;
        RecursiveHelper& operator=(RecursiveHelper      &&) = default;
        ~RecursiveHelper() = default;

        template <class Op, class... Args>
        constexpr decltype(auto) apply(Op const& op, Args&&... args) const noexcept(noexcept(op.leaf(std::declval<F const&>(), std::forward<Args>(args)...))) {
          return op.leaf(f(), std::forward<Args>(args)...);
        }
    };
    template <class Dummy, class F, class F2, class... Fs>
    struct RecursiveHelper<Dummy, F, F2, Fs...> : private wrap<RecursiveHelper<void, F2, Fs...>, F, F2, Fs...>, private wrap<F, F, F2, Fs...> {
      private:
        using         base_t = RecursiveHelper<void, F2, Fs...>;
        using      wrapper_t = wrap<F     , F, F2, Fs...>;
        using base_wrapper_t = wrap<base_t, F, F2, Fs...>;
        constexpr decltype(auto) f   () const { return static_cast<     wrapper_t const&>(*this).get(); }
        constexpr decltype(auto) base() const { return static_cast<base_wrapper_t const&>(*this).get(); }
      public:
        template <class G, class G2, class...Gs, class = std::enable_if_t<sizeof...(Fs) == sizeof...(Gs)>>
        constexpr RecursiveHelper(G&& g, G2&& g2, Gs&&... gs) : base_wrapper_t(std::forward<G2>(g2), std::forward<Gs>(gs)...), wrapper_t(std::forward<G>(g)) {}
        constexpr RecursiveHelper() = default;
        constexpr RecursiveHelper(RecursiveHelper const& ) = default;
        constexpr RecursiveHelper(RecursiveHelper      &&) = default;
        RecursiveHelper& operator=(RecursiveHelper const& ) = default;
        RecursiveHelper& operator=(RecursiveHelper      &&) = default;
        ~RecursiveHelper() = default;

        template <class Op, class... Args>
        constexpr decltype(auto) apply(Op const& op, Args&&... args) const noexcept(noexcept(op.recurse(std::declval<F const&>(), std::declval<base_t const&>(), std::forward<Args>(args)...))) {
          return op.recurse(f(), base(), std::forward<Args>(args)...);
        }
    };
    template <class F, class F2, class... Fs>
    struct RecursiveHelper<std::enable_if_t<is_empty_optimizable<F>::value>, F, F2, Fs...> : private wrap<F, F, F2, Fs...>, private wrap<RecursiveHelper<void, F2, Fs...>, F, F2, Fs...> {
      private:
        using         base_t = RecursiveHelper<void, F2, Fs...>;
        using      wrapper_t = wrap<F     , F, F2, Fs...>;
        using base_wrapper_t = wrap<base_t, F, F2, Fs...>;
        constexpr decltype(auto) f   () const { return static_cast<     wrapper_t const&>(*this).get(); }
        constexpr decltype(auto) base() const { return static_cast<base_wrapper_t const&>(*this).get(); }
      public:
        template <class G, class G2, class...Gs, class = std::enable_if_t<sizeof...(Fs) == sizeof...(Gs)>>
        constexpr RecursiveHelper(G&& g, G2&& g2, Gs&&... gs) : wrapper_t(std::forward<G>(g)), base_wrapper_t(std::forward<G2>(g2), std::forward<Gs>(gs)...) {}
        constexpr RecursiveHelper() = default;
        constexpr RecursiveHelper(RecursiveHelper const& ) = default;
        constexpr RecursiveHelper(RecursiveHelper      &&) = default;
        RecursiveHelper& operator=(RecursiveHelper const& ) = default;
        RecursiveHelper& operator=(RecursiveHelper      &&) = default;
        ~RecursiveHelper() = default;

        template <class Op, class... Args>
        constexpr decltype(auto) apply(Op const& op, Args&&... args) const noexcept(noexcept(op.recurse(std::declval<F const&>(), std::declval<base_t const&>(), std::forward<Args>(args)...))) {
          return op.recurse(f(), base(), std::forward<Args>(args)...);
        }
    };
  }



  struct Identity {
    constexpr Identity() = default;
    constexpr Identity(Identity const&) = default;
    Identity& operator=(Identity const&) = default;
    ~Identity() = default;

    template <class T>
    constexpr T operator()(T&& t) const noexcept(noexcept(T(std::forward<T>(t)))) {
      return std::forward<T>(t);
    }

    friend std::ostream& operator<<(std::ostream& out, Identity) {
      return out << "id";
    }
  };



  template <class T>
  class Value {
    private:
      T val{};
    public:
      template <class U, class = decltype(T(std::declval<U&&>()))>
      constexpr Value(U&& u) : val(std::forward<U>(u)) {}
      constexpr Value() = default;
      constexpr Value(Value const& ) = default;
      constexpr Value(Value      &&) = default;
      Value& operator=(Value const& ) = default;
      Value& operator=(Value      &&) = default;
      ~Value() = default;

      template <class... Args>
      constexpr decltype(auto) operator()(Args&&...) const noexcept(noexcept(T(val))) {
        return (val);
      }

      friend std::ostream& operator<<(std::ostream& out, Value const& c) {
        return out << c.val;
      }
  };

  template <class T>
  class Cast {
    public:
      template <class U>
      constexpr T operator()(U&& u) const noexcept(noexcept(T(std::declval<U>()))) {
        return std::forward<U>(u);
      }

      friend std::ostream& operator<<(std::ostream& out, Cast<T> const&) {
        return out << "cast";
      }
  };

  template <class Op, class F>
  class Applier : private Op, private F {
    private:
      constexpr F  const& f () const noexcept { return *this; }
      constexpr Op const& op() const noexcept { return *this; }
    public:
      template <class OP, class G, class = decltype(F(std::declval<G&&>()))>
      constexpr Applier(OP&& op, G&& g) : Op(std::forward<OP>(op)), F(std::forward<G>(g)) {}
      constexpr Applier() = default;
      constexpr Applier(Applier const& ) = default;
      constexpr Applier(Applier      &&) = default;
      Applier& operator=(Applier const& ) = default;
      Applier& operator=(Applier      &&) = default;
      ~Applier() = default;

      template <class... Args>
      constexpr decltype(auto) operator()(Args&&... args) const noexcept(noexcept(std::declval<Op const&>()(std::declval<F const&>()(std::forward<Args>(args)...)))) {
        return op()(f()(std::forward<Args>(args)...));
      }

      friend std::ostream& operator<<(std::ostream& out, Applier const& applier) {
        return out << details::op_name(applier.op()) << applier.f();
      }
  };

  namespace details {
    template <class Op>
    struct CombinerCaller {
      Op const& op;
      template <class F, class... Args>
      constexpr decltype(auto) leaf(F const& f, Args&&... args) const noexcept(noexcept(f(std::forward<Args>(args)...))) {
        return f(std::forward<Args>(args)...);
      }
      template <class F, class Base, class... Args>
      constexpr decltype(auto) recurse(F const& f, Base const& base, Args&&... args) const noexcept(noexcept(std::declval<Op const&>()(f(args...), base.apply(std::declval<CombinerCaller const&>(), args...)))) {
        return op(f(args...), base.apply(*this, args...));
      }
    };

    template <>
    struct CombinerCaller<std::logical_and<>> {
      std::logical_and<> const& op;
      template <class F, class... Args>
      constexpr decltype(auto) leaf(F const& f, Args&&... args) const noexcept(noexcept(f(std::forward<Args>(args)...))) {
        return f(std::forward<Args>(args)...);
      }
      template <class F, class Base, class... Args>
      constexpr decltype(auto) recurse(F const& f, Base const& base, Args&&... args) const noexcept(noexcept(0 ? f(args...) : base.apply(std::declval<CombinerCaller const&>(), std::forward<Args>(args)...))) {
        decltype(auto) res_f = f(args...);
        return unmove(res_f) ? base.apply(*this, std::forward<Args>(args)...) : res_f;
      }
    };

    template <>
    struct CombinerCaller<std::logical_or<>> {
      std::logical_or<> const& op;
      template <class F, class... Args>
      constexpr decltype(auto) leaf(F const& f, Args&&... args) const noexcept(noexcept(f(std::forward<Args>(args)...))) {
        return f(std::forward<Args>(args)...);
      }
      template <class F, class Base, class... Args>
      constexpr decltype(auto) recurse(F const& f, Base const& base, Args&&... args) const noexcept(noexcept(0 ? f(args...) : base.apply(std::declval<CombinerCaller const&>(), std::forward<Args>(args)...))) {
        decltype(auto) res_f = f(args...);
        return unmove(res_f) ? res_f : base.apply(*this, std::forward<Args>(args)...);
      }
    };
  }
  template <class Op, class... Fs>
  class Combiner : private Op, private details::RecursiveHelper<void, Fs...> {
    private:
      using helper_t = details::RecursiveHelper<void, Fs...>;
      constexpr Op       const& op    () const { return *this; }
      constexpr helper_t const& helper() const { return *this; }

      using Call = details::CombinerCaller<Op>;

      struct Print {
        Op const& op;
        template <class F>
        std::ostream& leaf(F const& f, std::ostream& out) const {
          return out << "(" << f << ")";
        }
        template <class F, class Base>
        std::ostream& recurse(F const& f, Base const& base, std::ostream& out) const {
          return base.apply(*this, out << "(" << f << ") " << details::op_name(op) << " ");
        }
      };
    public:
      template <class OP, class... Gs, class = std::enable_if_t<sizeof...(Fs) == sizeof...(Gs)>>
      constexpr Combiner(OP&& op, Gs&&... gs) : Op(std::forward<OP>(op)), helper_t(std::forward<Gs>(gs)...) {}
      constexpr Combiner() = default;
      constexpr Combiner(Combiner const& ) = default;
      constexpr Combiner(Combiner      &&) = default;
      Combiner& operator=(Combiner const& ) = default;
      Combiner& operator=(Combiner      &&) = default;
      ~Combiner() = default;

      template <class... Args>
      constexpr decltype(auto) operator()(Args&&... args) const noexcept(noexcept(std::declval<helper_t const&>().apply(std::declval<Call>(), std::forward<Args>(args)...))) {
        return helper().apply(Call{op()}, std::forward<Args>(args)...);
      }

      friend std::ostream& operator<<(std::ostream& out, Combiner const& combiner) {
        return combiner.helper().apply(Print{combiner.op()}, out);
      }
  };

  template <class... Fs>
  class Chain : private details::RecursiveHelper<void, Fs...> {
    private:
      using helper_t = details::RecursiveHelper<void, Fs...>;
      constexpr helper_t const& helper() const { return *this; }

      struct Call {
        template <class F, class... Args>
        constexpr decltype(auto) leaf(F const& f, Args&&... args) const noexcept(noexcept(f(std::forward<Args>(args)...))) {
          return f(std::forward<Args>(args)...);
        }
        template <class F, class Base, class... Args>
        constexpr decltype(auto) recurse(F const& f, Base const& base, Args&&... args) const noexcept(noexcept(f(base.apply(std::declval<Call const&>(), std::forward<Args>(args)...)))) {
          return f(base.apply(*this, std::forward<Args>(args)...));
        }
      };

      struct Print {
        template <class F>
        std::ostream& leaf(F const& f, std::ostream& out) const {
          return out << f;
        }
        template <class F, class Base>
        std::ostream& recurse(F const& f, Base const& base, std::ostream& out) const {
          return base.apply(*this, out << f << ", ");
        }
      };
    public:
      template <class... Gs, class = std::enable_if_t<sizeof...(Fs) == sizeof...(Gs)>>
      constexpr Chain(Gs&&... gs) : helper_t(std::forward<Gs>(gs)...) {}
      constexpr Chain() = default;
      constexpr Chain(Chain const& ) = default;
      constexpr Chain(Chain      &&) = default;
      Chain& operator=(Chain const& ) = default;
      Chain& operator=(Chain      &&) = default;
      ~Chain() = default;

      template <class... Args>
      constexpr decltype(auto) operator()(Args&&... args) const noexcept(noexcept(std::declval<helper_t const&>().apply(std::declval<Call>(), std::forward<Args>(args)...))) {
        return helper().apply(Call{}, std::forward<Args>(args)...);
      }

      friend std::ostream& operator<<(std::ostream& out, Chain const& combiner) {
        return combiner.helper().apply(Print{}, out << "compose(") << ")";
      }
  };

  template <class F, class S = std::string>
  class Func : private details::wrap<F>, private details::wrap<S> {
    private:
      using func_t = details::wrap<F>;
      using name_t = details::wrap<S>;
      constexpr decltype(auto) name() const noexcept { return static_cast<name_t const&>(*this).get(); }
      constexpr decltype(auto) f   () const noexcept { return static_cast<func_t const&>(*this).get(); }
    public:
      using wrapped_t = F;

      template <class Str, class G, class = decltype(F(std::declval<G&&>()))>
      constexpr Func(Str&& name, G&& g) : func_t(std::forward<G>(g)), name_t(std::forward<Str>(name)) {}

      template <class Str, class S2>
      constexpr Func(Str&& name, Func<F, S2> const&  g) : func_t(g.f()), name_t(std::forward<Str>(name)) {}
      template <class Str, class S2>
      constexpr Func(Str&& name, Func<F, S2>      && g) : func_t(std::move(g.f())), name_t(std::forward<Str>(name)) {}
      constexpr Func() = delete;
      constexpr Func(Func const& ) = default;
      constexpr Func(Func      &&) = default;
      Func& operator=(Func const& ) = default;
      Func& operator=(Func      &&) = default;
      ~Func() = default;

      template <class... Args>
      constexpr decltype(auto) operator()(Args&&... args) const noexcept(noexcept(std::declval<F>()(std::forward<Args>(args)...))) {
        return f()(std::forward<Args>(args)...);
      }

      friend std::ostream& operator<<(std::ostream& out, const Func& func) {
        return out << func.name();
      }
  };

  template <class T> class Functor;

  namespace details {

    template <class T>
    struct is_func : std::false_type {};
    template <>
    struct is_func<Identity> : std::true_type {};
    template <class T>
    struct is_func<Value<T>> : std::true_type {};
    template <class T>
    struct is_func<Cast<T>> : std::true_type {};
    template <class Op, class F>
    struct is_func<Applier<Op, F>> : std::true_type {};
    template <class Op, class... Fs>
    struct is_func<Combiner<Op, Fs...>> : std::true_type {};
    template <class... Fs>
    struct is_func<Chain<Fs...>> : std::true_type {};
    template <class F, class S>
    struct is_func<Func<F, S>> : std::true_type {};
    template <class F>
    struct is_func<Functor<F>> : std::true_type {};

    template <class...>
    struct any_is_func : std::false_type {};
    template <class Head, class... Tail>
    struct any_is_func<Head, Tail...> : std::conditional_t<is_func<Head>::value, std::true_type, any_is_func<Tail...>> {};

    template <class... Ts>
    using enable_any_is_func = std::enable_if_t<any_is_func<std::decay_t<Ts>...>::value>;

    template <class T>
    using requires_func = if_valid<T, std::enable_if_t<is_func<std::decay_t<T>>::value>>;

    namespace func_tag {
      struct scalar {};
      struct fun_ptr {};
      struct callable {};
      struct func {};
      struct func_wrapper {};
      struct functor {};

      namespace {
        constexpr scalar tag(...) { return {}; }

        template <class Ret, class... Args>
        constexpr fun_ptr tag(Ret (*)(Args...)) { return {}; }

        template <class T, class = std::enable_if_t<is_callable<std::decay_t<T>>::value && !is_func<std::decay_t<T>>::value>>
        constexpr callable tag(T const&) { return {}; }

        template <class T>
        constexpr func tag(requires_func<T> const&) { return {}; }

        template <class F, class S>
        constexpr func_wrapper tag(Func<F, S> const&) { return {}; }

        template <class Ret, class... Args>
        constexpr functor tag(Functor<Ret(Args...)> const&) { return {}; }
      }
    }

    struct anonymous {
      template <class T>
      constexpr anonymous(T&&) {}
      constexpr anonymous() = default;
      constexpr anonymous(const anonymous&) = default;
      ~anonymous() = default;
      friend std::ostream& operator<<(std::ostream& out, anonymous) {
        return out << "[anonymous]";
      }
    };

    namespace {
      template <class S, class T>
      constexpr auto ensure_func(func_tag::scalar, S&&, T&& t) {
        return Value<std::decay_t<T>>(std::forward<T>(t));
      }

      template <class S, class Ret, class... Args>
      constexpr auto ensure_func(func_tag::fun_ptr, S&& name, Ret (*f)(Args...)) {
        return Func<Ret(*)(Args...), std::decay_t<S>>(std::forward<S>(name), f);
      }

      template <class S, class F>
      constexpr auto ensure_func(func_tag::callable, S&& name, F&& f) {
        return Func<std::decay_t<F>, std::decay_t<S>>(std::forward<S>(name), std::forward<F>(f));
      }

      template <class S, class F>
      constexpr decltype(auto) ensure_func(func_tag::func, S&&, F&& f) {
        return std::forward<F>(f);
      }

      template <class S, class F>
      constexpr decltype(auto) ensure_func(func_tag::func_wrapper, S&&, F&& f) {
        return std::forward<F>(f);
      }

      template <class S, class F>
      constexpr decltype(auto) ensure_func(func_tag::functor, S&&, F&& f) {
        return std::forward<F>(f);
      }

      template <class T>
      constexpr decltype(auto) ensure_func(T&& t) {
        return ensure_func(func_tag::tag(t), anonymous{}, std::forward<T>(t));
      }

      template <class S, class T>
      constexpr auto create_func(func_tag::fun_ptr, S&& name, T&& t) {
        return ensure_func(func_tag::fun_ptr{}, std::forward<S>(name), std::forward<T>(t));
      }

      template <class S, class T>
      constexpr auto create_func(func_tag::callable, S&& name, T&& t) {
        return ensure_func(func_tag::callable{}, std::forward<S>(name), std::forward<T>(t));
      }

      template <class S, class T>
      constexpr auto create_func(func_tag::func_wrapper, S&& name, T&& t) {
        return Func<typename std::decay_t<T>::wrapped_t, std::decay_t<S>>(std::forward<S>(name), std::forward<T>(t));
      }

      template <class Tag, class S, class T>
      constexpr auto create_func(Tag, S&& name, T&& t) {
        using Str = std::decay_t<S>;
        decltype(auto) f = ensure_func(Tag{}, Str(""), std::forward<T>(t));
        return Func<std::decay_t<decltype(f)>, Str>(std::forward<S>(name), std::move(f));
      }


      template <class Op, class... Fs>
      constexpr auto combine_helper(Op&& op, Fs&&... fs) {
        return Combiner<std::decay_t<Op>, std::decay_t<Fs>...>(std::forward<Op>(op), std::forward<Fs>(fs)...);
      }
      template <class Op, class... Fs>
      constexpr auto combine(Op&& op, Fs&&... fs) {
        return combine_helper(std::forward<Op>(op), ensure_func(std::forward<Fs>(fs))...);
      }
    }

  }

  template <class S, class T>
  constexpr auto func(S&& name, T&& t) {
    return details::create_func(details::func_tag::tag(t), std::forward<S>(name), std::forward<T>(t));
  }

  template <class T>
  constexpr auto func(T&& t) {
    return details::ensure_func(std::forward<T>(t));
  }

  template <class Ret, class... Args>
  class Functor<Ret(Args...)> {
    private:
      class base {
        public:
          virtual ~base() {}
          virtual Ret operator()(Args...) const = 0;
          virtual std::ostream& print(std::ostream&) const = 0;
          virtual std::unique_ptr<base> clone() const = 0;
      };

      template <class F>
      class impl : public base {
        private:
          F f;
        public:
          template <class G, class = decltype(F(std::declval<G&&>()))>
          impl(G&& g) : f(std::forward<G>(g)) {}

          Ret operator()(Args... args) const override {
            return f(static_cast<Args>(args)...);
          }

          std::ostream& print(std::ostream& out) const override {
            return out << f;
          }

          std::unique_ptr<base> clone() const override {
            return std::make_unique<impl>(f);
          }
      };

      std::unique_ptr<base> fp;

    public:
      template <class G, class F = std::decay_t<G>, class = decltype(Ret(std::declval<F const&>()(std::declval<Args>()...))), class = std::enable_if_t<!std::is_same<Functor, std::decay_t<G>>::value>>
      Functor(details::requires_func<G>&& g) : fp(std::make_unique<impl<F>>(std::forward<G>(g))) {}

      Functor() = delete;
      Functor(Functor const&  other) : fp(other.fp->clone()) {}
      Functor(Functor      && other) = default;
      Functor& operator=(Functor const&  other) {
        fp = other.clone();
        return *this;
      }
      Functor& operator=(Functor      && other) = default;
      ~Functor() = default;

      Ret operator()(Args... args) const {
        return (*fp)(static_cast<Args>(args)...);
      }

      friend std::ostream& operator<<(std::ostream& out, Functor const& functor) {
        return functor.fp->print(out);
      }
  };


  // Arithmetic operators
  template <class F>
  constexpr auto operator+(details::requires_func<F>&& f) {
    return std::forward<F>(f);
  }

  template <class F>
  constexpr auto operator-(details::requires_func<F>&& f) {
    using Op = std::negate<>;
    return Applier<Op, std::decay_t<F>>(Op{}, std::forward<F>(f));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator+(F&& f, G&& g) {
    return details::combine(std::plus<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator-(F&& f, G&& g) {
    return details::combine(std::minus<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator*(F&& f, G&& g) {
    return details::combine(std::multiplies<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator/(F&& f, G&& g) {
    return details::combine(std::divides<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator%(F&& f, G&& g) {
    return details::combine(std::modulus<>{}, std::forward<F>(f), std::forward<G>(g));
  }


  // Comparisons
  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator==(F&& f, G&& g) {
    return details::combine(std::equal_to<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator!=(F&& f, G&& g) {
    return details::combine(std::not_equal_to<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator>(F&& f, G&& g) {
    return details::combine(std::greater<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator<(F&& f, G&& g) {
    return details::combine(std::less<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator>=(F&& f, G&& g) {
    return details::combine(std::greater_equal<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator<=(F&& f, G&& g) {
    return details::combine(std::less_equal<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  // Logical operators
  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator||(F&& f, G&& g) {
    return details::combine(std::logical_or<>{}, details::ensure_func(std::forward<F>(f)), details::ensure_func(std::forward<G>(g)));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator&&(F&& f, G&& g) {
    return details::combine(std::logical_and<>{}, details::ensure_func(std::forward<F>(f)), details::ensure_func(std::forward<G>(g)));
  }

  template <class F>
  constexpr auto operator!(details::requires_func<F>&& f) {
    using Op = std::logical_not<>;
    return Applier<Op, std::decay_t<F>>(Op{}, std::forward<F>(f));
  }

  // Bit operators
  template <class F>
  constexpr auto operator~(details::requires_func<F>&& f) {
    using Op = std::bit_not<>;
    return Applier<Op, std::decay_t<F>>(Op{}, std::forward<F>(f));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator&(F&& f, G&& g) {
    return details::combine(std::bit_and<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator|(F&& f, G&& g) {
    return details::combine(std::bit_or<>{}, std::forward<F>(f), std::forward<G>(g));
  }

  template <class F, class G, class = details::enable_any_is_func<F, G>>
  constexpr auto operator^(F&& f, G&& g) {
    return details::combine(std::bit_xor<>{}, std::forward<F>(f), std::forward<G>(g));
  }



  template <class... Fs>
  constexpr auto compose(details::requires_func<Fs>&&... fs) {
    return Chain<std::decay_t<Fs>...>(std::forward<Fs>(fs)...);
  }

  template <class... Fs, class = details::enable_any_is_func<Fs...>>
  constexpr auto sum(Fs&&... fs) {
    return details::combine(std::plus<>{}, std::forward<Fs>(fs)...);
  }

  template <class... Fs, class = details::enable_any_is_func<Fs...>>
  constexpr auto product(Fs&&... fs) {
    return details::combine(std::multiplies<>{}, std::forward<Fs>(fs)...);
  }

  template <class... Fs, class = details::enable_any_is_func<Fs...>>
  constexpr auto conjunction(Fs&&... fs) {
    return details::combine(std::logical_and<>{}, std::forward<Fs>(fs)...);
  }

  template <class... Fs, class = details::enable_any_is_func<Fs...>>
  constexpr auto disjunction(Fs&&... fs) {
    return details::combine(std::logical_or<>{}, std::forward<Fs>(fs)...);
  }

} // namespace functors
